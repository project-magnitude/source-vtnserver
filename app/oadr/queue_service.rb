require 'thread'

class QueueService
  @@report_queue = Queue.new
  @@event_queue = Queue.new

  def report_queue
    @@report_queue
  end

  def event_queue
    @@event_queue
  end
end