require 'app/oadr/queue_service'

class ApisController < BaseController
  # http_basic_authenticate_with name: "test", password: "test"
  before_filter :authenticate #bind the correct account in @account

  def create_report
    #get params from request
    ven_id = params[:ven_id]
    report_params = params[:create_report]
    report_req_params = params[:report_request]

    #Create report
    create_report = _create_report(ven_id, report_params)
    if create_report
      #Create report reqeust
      create_report_id = create_report.id
      report_request = _create_report_request(create_report_id, ven_id, report_req_params)
      if report_request
        #Send report
        _send_report(create_report_id)
        render json: {message: 'Report send', report: create_report, report_request: report_request}, status: 200
      else
        render json: {message: 'Create report request error'}, status: 500
      end
    else
      render json: {message: 'Create report error'}, status: 500
    end
  rescue => exception
    render json: {message: 'Generic error', errors: exception.backtrace}, status: 500
  end

  def showReports
    queue = QueueService.new.report_queue

    if queue.empty?
      render json: {report: {}}, status: 200
    else
    report_instance = queue.pop
    report_intervals = ReportInterval.find_all_by_report_instance_id(report_instance.id)

    class << report_instance
      attr_accessor :intervals

      def as_json(options)
        super(:methods => :intervals)
      end
    end

    report_instance.intervals = report_intervals
    render json: {report: report_instance}, status: 200
    end
  rescue => exception
    render json: {message: 'Generic error', errors: exception.backtrace}, status: 500
  end


  ### Events

  # event[dtstart_str]:2017-04-20 10:44:33 CEST
  # event[duration]:1
  # event[market_context]:pippo
  # event[priority]:0
  # event[response_required_type_id]:1
  # event[vtn_comment]:
  # event[test_event]:false
  # event[signal_name]:simple
  # event[signal_type]:delta
  # event[payload_value]:
  # ven[id][]:94dac05a2dbdd4773291
  # //ven[id][]:0d7459168ca51a425837
  # signals[][signal_name]:
  # signals[][signal_type]:
  # signals[][emix_unit_id]:2
  # signals[][dtstart]:2017-03-22 16:27:04 UTC
  # signals[][duration]:1
  # signals[][baseline_name]:baseline_pippo
  # signals[][intervals][][duration]:1
  # signals[][intervals][][uid]:0
  # signals[][intervals][][payload]:10
  # signals[][intervals][][payload_type_id]:
  # signals[][intervals][][duration]:1
  # signals[][intervals][][uid]:1
  # signals[][intervals][][payload]:17
  # signals[][intervals][][payload_type_id]:
  # signals[][signal_name]:LOAD_DISPATCH
  # signals[][signal_type]:setpoint
  # signals[][emix_unit_id]:2
  # signals[][dtstart]:
  # signals[][duration]:
  # //signals[][baseline_name]:
  # signals[][intervals][][duration]:1
  # signals[][intervals][][uid]:0
  # signals[][intervals][][payload]:10
  # signals[][intervals][][payload_type_id]:
  # signals[][intervals][][duration]:1
  # signals[][intervals][][uid]:1
  # signals[][intervals][][payload]:17
  # signals[][intervals][][payload_type_id]:

  def create_event
    # events_controller.create

    signal_type = params[:event][:signal_type]
    signal_name = params[:event][:signal_name]
    payload_value = params[:event][:payload_value]

    signal_type_id = SignalType.find_by_name(signal_type).id
    signal_name_id = SignalName.find_by_name(signal_name).id

    params[:event].delete(:signal_type)
    params[:event].delete(:signal_name)
    params[:event].delete(:payload_value)

    market_context = MarketContext.find_by_name(params[:event][:market_context])
    params[:event][:market_context_id] = market_context.id
    params[:event].delete(:market_context)

    @event = Event.new(params[:event])

    # ficristo: the current account is always an admin

    # @event.test_event = "test event: #{current_account.name}" if !current_account_is_admin

    # signal_type_id = @event.signal_type_id #|| SignalType.first.id

    # account = (current_account_is_admin ? nil : current_account)
    account = nil

    if Event.default_event(@event, signal_name_id, signal_type_id, payload_value, account)

      if @event.dtstart != nil && @event.dtstart < DateTime.now
        @event.event_status = EventStatus.find_by_name("active")
        @event.save
      end

      # Event.default_event creates also a new signal, we don't need it for now
      # event_signals_controller.destroy
      @event_signal = @event.event_signals[0]
      @event_signal.destroy

      # events_controller.add_vens
      begin
        vens = params[:ven][:id]

        vens.each do |ven_id|
          ven = @account.vens.find_by_ven_id(ven_id)
          event_ven = @event.event_vens.new
          event_ven.ven_id = ven.id

          event_ven.save
        end

      rescue NoMethodError => exception
        render json: {message: 'Create event add_vens NoMethodError error', errors: exception.backtrace}, status: 500
      else
        signals = params[:signals] || []
        signals.each do |signal| # params[:event_signal]
        # event_signals_controller.create
          signal_name = SignalName.find_by_name(signal[:signal_name])
          signal_type = SignalType.find_by_name(signal[:signal_type])
          signal[:signal_name_id] = signal_name.nil?? nil : signal_name.id
          signal[:signal_type_id] = signal_type.nil?? nil : signal_type.id
          signal.delete(:signal_name)
          signal.delete(:signal_type)
          if !signal[:end_device_asset].nil?
            end_device_asset = EndDeviceAsset.find_by_name(signal[:end_device_asset])
            signal[:end_device_asset_id] = end_device_asset.id
            signal.delete(:end_device_asset)
          end
          @event_signal = @event.event_signals.build(signal)

          # use submitted a new baseline
          if signal[:baseline_name]
            @event_signal.default_baseline
          else
            @event_signal.signal_id = SecureRandom.hex(10)
          end

          if !@event_signal.save
            render json: {message: 'Create event signal unprocessable entity error', errors: @event_signal.errors.messages}, status: 500
          else
            intervals = signal[:intervals]
            intervals.each do |interval|
              # event_signal_intervals_controller.create
              @event_signal_interval = @event_signal.event_signal_intervals.build(interval)

              if !@event_signal_interval.save
                render json: {message: 'Create event signal interval unprocessable entity error', errors: @event_signal_interval.errors.messages}, status: 500
              end
            end
          end
        end

        # events_controller.publish
        if @event.publish
          render json: {message: 'Create event', event: @event}, status: 200
        else
          render json: {message: 'Failed to publish event.', errors: @event.errors.messages}, status: 500
        end
      end
    else
      render json: {message: 'Create event unprocessable entity error', errors: @event.errors.messages}, status: 500
    end
  rescue => exception
    render json: {message: 'Generic error', errors: exception.backtrace}, status: 500
  end

  def show_events
    queue = QueueService.new.event_queue

    if queue.empty?
      render json: {event: {}}, status: 200
    else
      event_opt = queue.pop

      event = Event.find_by_id(event_opt.event_id)
      opt_type = OptType.find_by_id(event_opt.opt_type_id)
      if !event_opt.opt_reason_id.nil?
        opt_reason = OptReason.find_by_id(event_opt.opt_reason_id)
      end
      render json: {event: event, event_opt: event_opt, opt_type: opt_type, opt_reason: opt_reason}, status: 200
    end
  rescue => exception
    render json: {message: 'Generic error', errors: exception.backtrace}, status: 500
  end

  def cancel_event
    event_id = params[:event_id]
    event = Event.where(["event_id = ?", event_id])[0]
    result = _cancel_event(event)
    if result
      # events_controller.publish
      if event.publish
        render json: {message: 'Event cancelled.'}, status: 200
      else
        render json: {message: 'Failed to publish event cancellation.', errors: event.errors.messages}, status: 500
      end
    else
      render json: {message: 'Cannot cancel the event.'}, status: 500
    end
  rescue => exception
    render json: {message: 'Generic error', errors: exception.backtrace}, status: 500
  end


  private

  def _create_report(ven_id, report_params)
    #create_reports_controller.create
    ven = @account.vens.find_by_ven_id(ven_id)
    create_report = ven.create_reports.build(report_params)
    create_report.request_id = SecureRandom.hex(10)
    if create_report.save
      create_report
    end
  end

  def _create_report_request(create_report_id, ven_id, report_req_params)
    #report_requests_controller.create
    create_report = CreateReport.find(create_report_id)

    id = @account.vens.find_by_ven_id(ven_id).id
    report = Report.joins(:report_name).where(reports: {ven_id: id}, report_names:{name: report_req_params['report_name']})[0]
    report_req_params['report_id'] = report.id

    report_request = create_report.report_requests.build(report_req_params)
    report_request.request_id = SecureRandom.hex(10)
    if report_request.save
      report_request
    end
  end

  def _send_report(report_id)
    #create_reports_controller.create
    create_report = CreateReport.find(report_id)
    create_report.queue_create_report
  end

  def _cancel_event(event)
    # events_controller.cancel
    # self.with_lock do
      return false if event.event_status.name == "completed"

      event.event_status = EventStatus.find_by_name("cancelled")
      event.save!

      event.unpublish
    # end

    true
  end
end