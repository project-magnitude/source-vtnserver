=====================================================
Source Codeof VTN server implementation - v1.0
=====================================================


.:: CONTENTS ::.

This project contains source code for the VTN Server


.:: SETUP ::.

BUILD APPLICATION

Execute following commands:

   RUN jruby -J-Xmx1024m -S bundle install
   rake assets:precompile
   bundle exec puma -C config/puma.rb


DATABASE INITIALISATION

Execute following commands:
	rake db:setup RAILS_ENV=production
	rake db:seed


.:: ACCESS TO THE VTN SERVER GUI ::.

The EPRI VTN server implementation provides a GUI that is available at:
	localhost:8383
Credentials:
	user: admin
	password: testing


.:: CONFIGURATION ::.

The key parameters to configure for setting-up the VTN are the following:
- VTN server: using the VTN GUI, create a new VEN by defining:
	1) name of the VEN
	2) VEN ID (automatically generated)
	3) market context (to be created before its selection in the VEN options)
	4) schema version (2.0b)


.:: CHANGELOG ::.

v1.0 Tested on the development machine


.:: CONTACTS ::.

alessio.roppolo@eng.it
